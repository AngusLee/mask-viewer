const maskPixelLUT = [
    0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80,
    0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, 0x8000,
    0x10000, 0x20000, 0x40000, 0x80000, 0x100000, 0x200000, 0x400000, 0x800000,
    0x1000000, 0x2000000, 0x4000000, 0x8000000, 0x10000000, 0x20000000, 0x40000000, 0x80000000
];

class Viewer {

    constructor(canvas, width, height) {
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
        this.pixelRatio = 2;
        this.width = width * this.pixelRatio;
        this.height = height * this.pixelRatio;
        this.tileW = this.width / 8;
        this.tileH = this.height / 4;
        this.resize();
    }

    resize() {
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.canvas.style.width = `${this.width / 2}px`;
        this.canvas.style.height = `${this.height / 2}px`;
    }

    clear() {
        this.context.fillStyle = '#000';
        this.context.fillRect(0, 0, this.width, this.height);
    }

    display(number) {
        console.log(number);
        let R = (Math.random() * 255) | 0;
        let G = (Math.random() * 255) | 0;
        let B = (Math.random() * 255) | 0;
        this.context.fillStyle = `rgb(${R}, ${G}, ${B})`;
        for(let r = 0; r < 4; ++r) {
            for(let c = 0; c < 8; ++c) {
                let i = r * 8 + c;
                if ((number & maskPixelLUT[i]) !== 0) {
                    let x = c * this.tileW;
                    let y = r * this.tileW;
                    this.context.fillRect(x, y, this.tileW, this.tileH);
                }
            }
        }
    }

}

function main() {
    let viewer = new Viewer(document.getElementById('canvas'), 480, 240);
    viewer.clear();

    let convert_button = document.getElementById('convert');
    let clear_botton = document.getElementById('clear');
    let input = document.getElementById('input');
    convert_button.onclick = function() {
        const number = parseInt(input.value);
        viewer.display(number);
    }
    clear_botton.onclick = viewer.clear.bind(viewer);
}

main();